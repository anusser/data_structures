#include "unit_tests.h"

#include "ch_traversal.h"
#include "defs.h"
#include "dijkstra.h"
#include "exporter.h"
#include "flags.h"
#include "geometry.h"
#include "graph.h"
#include "interval_tree.h"
#include "merge_sorted_unique.h"
#include "naive_ch_pathfinder.h"
#include "naive_pathfinder.h"
#include "parser.h"
#include "path_generator.h"
#include "path_parser.h"
#include "pathfinder.h"
#include "pathfinder_ds.h"
#include "paths.h"
#include "random.h"
#include "shortest_path_partitioner.h"

#include <algorithm>
#include <memory>
#include <string>
#include <unordered_set>

//
// helper functions/macros
//

#define Test(x)                                                                \
	do {                                                                       \
		if (!(x)) {                                                            \
			std::cout << "\n";                                                 \
			std::cout << "TEST_FAILED!\n";                                     \
			std::cout << "File: " << __FILE__ << "\n";                         \
			std::cout << "Line: " << __LINE__ << "\n";                         \
			std::cout << "Function: " << __func__ << "\n";                     \
			std::cout << "Test: " << #x << "\n";                               \
			std::cout << "\n";                                                 \
			std::cout << std::flush;                                           \
			std::abort();                                                      \
		}                                                                      \
	} while (0)

namespace
{

void printTestHeader(std::string const& test_name)
{
	std::string line(8 + test_name.length(), '-');

	Print("Testing " << test_name);
	Print(line);
}

void printTestFooter(std::string const& test_name)
{
	std::string line(16 + test_name.length(), '=');

	Print("");
	Print(line);
	Print(test_name << " test successful");
	Print(line);
	Print("");
}

template <typename T>
bool checkEquality(
    std::vector<T> const& to_check, std::vector<T> const& baseline)
{
	std::vector<T> missing;
	std::set_difference(baseline.begin(), baseline.end(), to_check.begin(),
	    to_check.end(), std::inserter(missing, missing.begin()));
	std::vector<T> additional;
	std::set_difference(to_check.begin(), to_check.end(), baseline.begin(),
	    baseline.end(), std::inserter(additional, additional.begin()));

	if (!missing.empty() || !additional.empty()) {
		std::cout << "Non-matching vectors found!\n";
		std::cout << "Missing elements: ";
		for (auto m : missing) {
			std::cout << m << " ";
		}
		std::cout << "\n";
		std::cout << "Additional elements: ";
		for (auto a : additional) {
			std::cout << a << " ";
		}
		std::cout << "\n";
		return false;
	}

	return true;
}
}

//
// Tests
//

namespace pf
{

static Random random;

void unit_tests::testAll(unsigned int seed)
{
	random.seed(seed);
	testAll();
}

void unit_tests::testAll()
{
	Print("Executing all unit tests");
	Print("========================");
	Print("");
	Print("Using random seed value: " << random.getSeed());
	Print("");

	unit_tests::testIntervalTree();
	unit_tests::testMergeSortedUnique();
	unit_tests::testExporter();
	unit_tests::testPathParser();
	unit_tests::testRTree();
	unit_tests::testFlags();
	unit_tests::testParser();
	unit_tests::testGraph();
	unit_tests::testPaths();

	unit_tests::testPathGenerator();
	unit_tests::testRandom();
	unit_tests::testEdgeRange();
	unit_tests::testDijkstra();
	unit_tests::testNodeRange();
	unit_tests::testShortestPathPartitioner();
	unit_tests::testAmbiguousShopas();

	unit_tests::testPathfinderDS();
	unit_tests::testPathfinder(true);
	unit_tests::testPathfinder(false);
	unit_tests::testCHTraversal();
}

void unit_tests::testParser()
{
	printTestHeader("Parser");

	Nodes nodes;
	Edges edges;

	Parser parser(nodes, edges);
	bool success = parser.parse("../../data/test.graph");

	Test(success);

	Test(nodes.size() == 14951);
	Test(edges.size() == 54903);

	Test(nodes.front().id == 0);
	Test(nodes.front().osm_id == 27393353);
	Test(nodes.front().coordinate.lat == 48.9535070);
	Test(nodes.front().coordinate.lon == 8.8727831);
	Test(nodes.front().elevation == 285);
	Test(nodes.front().level == 0);
	Test(nodes.back().osm_id == 486562412);

	Test(edges.back().id == (EdgeID)edges.size() - 1);
	Test(edges.back().source == 14949);
	Test(edges.back().target == 5575);
	Test(edges.back().length == 60);
	Test(edges.back().type == 12);
	Test(edges.back().speed == 30);
	Test(edges.back().child_edge1 == -1);
	Test(edges.back().child_edge2 == -1);

	printTestFooter("Parser");
}

void unit_tests::testGraph()
{
	printTestHeader("Graph");

	Graph graph;
	graph.init("../../data/test.graph");

	Test(graph.getNumberOfNodes() == 14951);
	Test(graph.getNumberOfEdges() == 54903);

	std::size_t count = 0;
	for (auto const& edge : graph.getOutEdgesOf(0)) {
		Test(edge.target == 6588 || edge.target == 11947);
		++count;
	}
	Test(count == 2);

	count = 0;
	for (auto const& edge : graph.getOutEdgesOf(14873).reverse()) {
		Test(edge.target == 14871 || edge.target == 14872 ||
		     edge.target == 14875);
		++count;
	}
	Test(count == 3);

	count = 0;
	for (auto const& edge : graph.getInEdgesOf(14932)) {
		Test(edge.source == 14930 || edge.source == 14933 ||
		     edge.source == 14934 || edge.source == 14935 ||
		     edge.source == 14937);
		Test(edge.source == edge.getHead(Direction::Backward));
		++count;
	}
	Test(count == 5);

	Test(graph.getNode(42).osm_id == 28287688);
	Test(graph.getEdge(19).target == 11072);

	Test(graph.getNumberOfOutEdges(14944) == 4);
	Test(graph.getNumberOfOutEdges(14948) == 4);

	Test(graph.getNumberOfInEdges(14944) == 4);

	//
	// Check if the edges reported by getParents are correct
	//

	for (std::size_t i = 0; i < 1000; ++i) {
		auto edge_id = random.getEdge(graph);
		auto function_parents = graph.getParents(edge_id);

		EdgeIDs real_parents;
		for (auto const& edge : graph.getAllEdges()) {
			if (edge.child_edge1 == edge_id || edge.child_edge2 == edge_id) {
				real_parents.push_back(edge.id);
			}
		}

		std::sort(function_parents.begin(), function_parents.end());
		std::sort(real_parents.begin(), real_parents.end());

		Test(function_parents == real_parents);
	}

	//
	// Check up an down edge iterators
	//

	for (std::size_t i = 0; i < 1000; ++i) {
		auto node_id = random.getNode(graph);

		for (auto direction : {Direction::Forward, Direction::Backward}) {
			std::size_t edge_count = 0;

			for (auto const& edge : graph.getUpEdgesOf(node_id, direction)) {
				Test(edge.getTail(direction) == node_id);
				++edge_count;
			}

			for (auto const& edge : graph.getDownEdgesOf(node_id, direction)) {
				Test(edge.getTail(direction) == node_id);
				++edge_count;
			}

			Test(graph.getNumberOfEdges(node_id, direction) == edge_count);
		}
	}

	//
	// Check for edges that are not shortest paths
	//

	std::size_t edge_count = 0;
	std::size_t shortcut_count = 0;

	CHDijkstra dijkstra(graph);

	for (auto const& edge : graph.out_edges) {
		dijkstra.run(edge.source, edge.target);
		auto const& result = dijkstra.getResult();

		if (result.path_length != edge.length) {
			if (edge.isShortcut()) {
				++shortcut_count;
			} else {
				++edge_count;
			}
		}
	}

	Print("There are " << edge_count << " normal edges and " << shortcut_count
	                   << " shortcuts which are no shortest paths.");

	printTestFooter("Graph");
}

void unit_tests::testPaths()
{
	printTestHeader("Paths");

	Graph graph;
	graph.init("../../data/test.graph");

	// The edges 0 and 1 do not match so an exception should be thrown.
	try {
		Path path(graph);
		path.push(0);
		path.push(1);
		Test(false);
	} catch (Exception const& e) {
		Test(true);
	}

	// The edges 8 and 13 match so no exception should be thrown.
	try {
		Path path(graph);
		path.push(8);
		path.push(13);
		Test(true);
	} catch (Exception const& e) {
		Test(false);
	}

	Path path(graph);
	path.push(8);
	path.push(13);
	path.push(16);
	Test(path.length() == 3);
	Test(path.getSource() == 2);
	Test(path.getTarget() == 11665);
	path.pop();
	Test(path.getEdges().size() == 2);
	for (auto edge_id : path) {
		Test(edge_id == 8 || edge_id == 13);
	}
	path.clear();
	Test(path.getEdges().size() == 0);

	Path time_path(graph);
	time_path.push(8);
	time_path.edges.back().time_intervall = TimeIntervall(10001, 10002);
	time_path.push(13);
	time_path.edges.back().time_intervall = TimeIntervall(10002, 10004);

	Path compressed_time_path(graph);
	compressed_time_path.push(9);
	compressed_time_path.edges.back().time_intervall =
	    TimeIntervall(10002, 10003);

	assert(graph.getEdge(9).child_edge1 == 8);
	assert(graph.getEdge(9).child_edge2 == 13);

	compressed_time_path.setTimeIntervallsForCompressed(time_path);
	assert(compressed_time_path.edges.back().time_intervall ==
	       TimeIntervall(10001, 10004));

	printTestFooter("Paths");
}

void unit_tests::testPathParser()
{
	printTestHeader("PathParser");

	Graph graph;
	graph.init("../../data/test.graph");

	Paths paths;
	PathParser path_parser(graph, paths);
	bool success = path_parser.parse("../../data/test_paths.json");

	Test(success);

	Test(paths.size() == 3);

	Test(paths[0].getEdges().size() == 3);
	Test(paths[1].getEdges().size() == 5);
	Test(paths[2].getEdges().size() == 4);

	Test(paths[0].getEdges().at(0).time_intervall.min_time == 1000000000);
	Test(paths[0].getEdges().at(0).time_intervall.max_time == 1000000003);
	Test(paths[0].getEdges().at(1).time_intervall.min_time == 1000000003);
	Test(paths[0].getEdges().at(1).time_intervall.max_time == 1000000006);
	Test(paths[0].getEdges().at(2).time_intervall.min_time == 1000000006);

	Test(paths[1].getEdges().at(2).time_intervall.min_time == 1000000004);
	Test(paths[1].getEdges().at(2).time_intervall.max_time == 1000000008);

	Test(paths[2].getEdges().at(3).time_intervall.min_time == 1000000007);
	Test(paths[2].getEdges().at(3).time_intervall.max_time == 1000000010);

	printTestFooter("PathParser");
}

void unit_tests::testPathGenerator()
{
	printTestHeader("PathGenerator");

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);
	for (std::size_t i = 0; i < 2000; ++i) {
		auto path_types = path_generator.getRandomPathTypes(10);
		auto path = path_generator.getStitchedPath(path_types);
	}

	printTestFooter("PathGenerator");
}

void unit_tests::testRandom()
{
	printTestHeader("Random");

	Graph graph;
	graph.init("../../data/test.graph");

	for (std::size_t i = 0; i < 20000; ++i) {
		auto node_id = random.getNode(graph);
		Test(node_id >= 0 && node_id < (NodeID)graph.getNumberOfNodes());

		auto edge_id = random.getEdge(graph);
		Test(edge_id >= 0 && edge_id < (EdgeID)graph.getNumberOfEdges());

		auto out_edge_id = random.getNonShortcutOutgoingEdge(graph, node_id);
		bool found = false;
		for (auto const& edge_id : graph.getNonShortcutOutEdgesOf(node_id)) {
			if (edge_id == out_edge_id) {
				found = true;
			}
		}

		Test(found ||
		     (!found && graph.getNonShortcutOutEdgesOf(node_id).size() == 0 &&
		         out_edge_id == c::NO_EID));

		int rand_int = random.getInt(23, 42);
		Test(rand_int >= 23 && rand_int < 42);

		float rand_float = random.getFloat(1.234, 5.6789);
		Test(rand_float >= 1.234 && rand_float <= 5.6789);
	}

	printTestFooter("Random");
}

void unit_tests::testEdgeRange()
{
	printTestHeader("EdgeRange");

	Graph graph;
	graph.init("../../data/test.graph");

	for (std::size_t i = 0; i < 1000; ++i) {
		auto node_id = random.getNode(graph);

		std::vector<EdgeID> ids;

		for (auto const& edge : graph.getOutEdgesOf(node_id)) {
			ids.push_back(edge.id);
		}

		for (auto const& edge : graph.getOutEdgesOf(node_id).reverse()) {
			ids.push_back(edge.id);
		}

		for (std::size_t i = 0; i < ids.size(); ++i) {
			Test(ids[i] == ids[ids.size() - i - 1]);
		}
	}

	printTestFooter("EdgeRange");
}

void unit_tests::testDijkstra()
{
	printTestHeader("Dijkstra");

	Graph graph;
	graph.init("../../data/test.graph");

	Dijkstra dijkstra(graph);
	CHDijkstra ch_dijkstra(graph);

	for (std::size_t i = 0; i < 2000; ++i) {
		auto source = random.getNode(graph);
		auto target = random.getNode(graph);

		dijkstra.run(source, target);
		ch_dijkstra.run(source, target);

		auto const& result = dijkstra.getResult();
		auto const& ch_result = ch_dijkstra.getResult();

		Test(result.path_found == ch_result.path_found);

		if (!result.path_found) {
			continue;
		}

		Length path_length = 0;
		for (PathEdge path_edge : result.path.getEdges()) {
			path_length += graph.getEdge(path_edge.edge_id).length;
		}
		Test(result.path_length == path_length);

		Test(result.path_length == ch_result.path_length);
	}

	printTestFooter("Dijkstra");
}

void unit_tests::testShortestPathPartitioner()
{
	printTestHeader("ShortestPathPartitioner");

	Graph graph;
	graph.init("../../data/test.graph");

	auto is_valid = [&](Path const& path, Path const& ch_path) {

		Test(path.getSource() == ch_path.getSource());
		Test(path.getTarget() == ch_path.getTarget());

		Length length_path = 0;
		for (PathEdge path_edge : path) {
			length_path += graph.getEdge(path_edge.edge_id).length;
		}

		Length length_ch_path = 0;
		for (PathEdge path_edge : ch_path) {
			length_ch_path += graph.getEdge(path_edge.edge_id).length;
		}

		Test(length_path == length_ch_path);
		Test(ch_path.unpack() == path);
	};

	PathGenerator path_generator(graph, random, nr_of_threads);

	//
	// PartitionerType::BINARY_SEARCH
	//
	std::unique_ptr<ShortestPathPartitioner> partitioner =
	    createPartitioner(graph, PartitionerType::BINARY_SEARCH);

	for (std::size_t i = 0; i < 1000; ++i) {
		auto path_types = PathGenerator::PathTypes(
		    10, PathGenerator::PathType::RandomShortest);
		auto path = path_generator.getStitchedPath(path_types);

		auto ch_path = partitioner->run(path);

		is_valid(path, ch_path);
	}

	//
	// PartitionerType::HIGHEST_LEVEL
	//

	// FIXME: comment in when it is finished
	// for (std::size_t i = 0; i < 1000; ++i) {
	//		auto path_types = PathGenerator::PathTypes(
	//	    	10, PathGenerator::PathType::RandomShortest);
	//		auto path = path_generator.getStitchedPath(path_types);
	//
	//     ShortestPathPartitioner partitioner(graph);
	//     auto ch_path = partitioner.run(path, PartitionerType::HIGHEST_LEVEL);
	//
	//     is_valid(path, ch_path);
	// }

	printTestFooter("ShortestPathPartitioner");
}

void unit_tests::testNodeRange()
{
	printTestHeader("NodeRange");

	Graph graph;
	PathGenerator path_generator(graph, random, nr_of_threads);

	graph.init("../../data/test.graph");

	std::vector<NodeID> node_range_nodes;
	for (std::size_t i = 0; i < 10000; ++i) {
		// Produce a random path
		auto path = path_generator.getRandomPath();

		if (path.length() == 0) {
			continue;
		}

		//
		// NodeRange over full path
		//

		// Get the nodes via a NodeRange loop
		node_range_nodes.clear();
		for (auto node_id : path.getNodeRange()) {
			node_range_nodes.push_back(node_id);
		}

		Test(node_range_nodes.size() == path.length() + 1);

		// Compare with the actual nodes
		std::size_t j = 0;
		Test(node_range_nodes[j++] == path.getSource());
		for (PathEdge path_edge : path) {
			auto const& edge = graph.getEdge(path_edge.edge_id);
			Test(edge.target == node_range_nodes[j++]);
		}

		//
		// NodeRange over part of the path
		//

		std::size_t begin = random.getInt(0, path.length());
		std::size_t end = random.getInt(begin, path.length());

		// Get the nodes via a NodeRange loop
		node_range_nodes.clear();
		for (auto node_id : path.getNodeRange(begin, end)) {
			node_range_nodes.push_back(node_id);
		}

		// Compare with the actual nodes
		std::size_t node_index = 0;
		for (std::size_t edge_index = begin; edge_index <= end; ++edge_index) {
			if (edge_index == path.length()) {
				Test(node_range_nodes[node_index] == path.getTarget());
			} else {
				auto const& edges = path.getEdges();
				auto const& edge = graph.getEdge(edges[edge_index].edge_id);
				Test(node_range_nodes[node_index] == edge.source);
			}

			++node_index;
		}

		//
		// ReverseNodeRange over full path
		//

		// Get the nodes via a ReverseNodeRange loop
		node_range_nodes.clear();
		for (auto node_id : path.getNodeRange().reverse()) {
			node_range_nodes.push_back(node_id);
		}

		Test(node_range_nodes.size() == path.length() + 1);

		// Compare with the actual nodes
		j = path.length();
		Test(node_range_nodes[j--] == path.getSource());
		for (PathEdge path_edge : path) {
			auto const& edge = graph.getEdge(path_edge.edge_id);
			Test(edge.target == node_range_nodes[j--]);
		}

		//
		// ReverseNodeRange over part of the path
		//

		begin = random.getInt(0, path.length());
		end = random.getInt(begin, path.length());

		// Get the nodes via a ReverseNodeRange loop
		node_range_nodes.clear();
		for (auto node_id : path.getNodeRange(begin, end).reverse()) {
			node_range_nodes.push_back(node_id);
		}

		// Compare with the actual nodes
		node_index = node_range_nodes.size() - 1;
		for (std::size_t edge_index = begin; edge_index <= end; ++edge_index) {
			if (edge_index == path.length()) {
				Test(node_range_nodes[node_index] == path.getTarget());
			} else {
				auto const& edges = path.getEdges();
				auto const& edge = graph.getEdge(edges[edge_index].edge_id);
				Test(node_range_nodes[node_index] == edge.source);
			}

			--node_index;
		}
	}

	printTestFooter("NodeRange");
}

void unit_tests::testPathfinderDS()
{
	printTestHeader("PathfinderDS");

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);
	auto paths = path_generator.getRandomPaths(100);

	PathfinderDS pathfinder_ds(graph, nr_of_threads);
	pathfinder_ds.init(std::move(paths), true);

	// test getEdgeBox
	for (std::size_t i = 0; i < 1000; ++i) {
		auto random_edge_id = random.getEdge(graph);
		auto path = graph.unpack(random_edge_id);

		auto const& ds_box = pathfinder_ds.getEdgeBox(random_edge_id);
		BoundingBox path_box(path);

		Test(ds_box == path_box);
	}

	// test getRNodeBox
	for (std::size_t i = 0; i < 1000; ++i) {
		auto random_node_id = random.getNode(graph);

		auto const& ds_box = pathfinder_ds.getRNodeBox(random_node_id);

		// set initial value in case random_node doesn't have down edges
		auto const& random_node = graph.getNode(random_node_id);
		BoundingBox manual_box(random_node.coordinate);
		// push down
		auto push_down_function = [&](Edge const& edge, Direction direction) {
			if (pathfinder_ds.is_obsolete[edge.id]) {
				return CHTraversal::Result::Ignore;
			}
			manual_box.expand(pathfinder_ds.getEdgeBox(edge.id));

			return CHTraversal::Result::Expand;
		};
		pathfinder_ds.traverse().down(
		    std::move(push_down_function), random_node_id);

		Test(ds_box == manual_box);
	}

	// test getREdgeBox
	// for (std::size_t i = 0; i < 1000; ++i) {
	//     auto random_node_id = random.getNode(graph);
	//
	//     auto const& ds_box = pathfinder_ds.getREdgeBox(random_node_id);
	//
	//     // check special case
	//     if (graph.getNumberOfEdges(random_node_id) == 0) {
	//         auto const& random_node = graph.getNode(random_node_id);
	//         Test(ds_box == BoundingBox(random_node.coordinate));
	//     }
	//
	//     // check "normal" case
	//     BoundingBox manual_box(
	//         pathfinder_ds.getBoxOfAdjacentEdges(random_node_id));
	//     auto push_down_function = [&](Edge const& edge, Direction direction)
	//     {
	//         auto head_id = edge.getHead(direction);
	//         manual_box.expand(pathfinder_ds.getBoxOfAdjacentEdges(head_id));
	//
	//         return CHTraversal::Result::Expand;
	//     };
	//     pathfinder_ds.traverse().down(
	//         std::move(push_down_function), random_node_id);
	//
	//     Test(ds_box == manual_box);
	// }

	// test isContainedInPath
	for (std::size_t i = 0; i < 1000; ++i) {
		auto random_edge_id = random.getEdge(graph);

		bool is_contained = false;
		for (auto const& path : pathfinder_ds.getPaths()) {
			for (PathEdge path_edge : path) {
				if (random_edge_id == path_edge.edge_id) {
					is_contained = true;
				}
			}
		}

		Test(is_contained == pathfinder_ds.isContainedInPath(random_edge_id));
	}

	// test getPathIDs
	for (std::size_t i = 0; i < 1000; ++i) {
		auto random_edge_id = random.getEdge(graph);

		PathIntervalls manual_path_intervalls;
		auto const& paths = pathfinder_ds.getPaths();

		for (PathID path_id = 0; path_id < (PathID)paths.size(); ++path_id) {
			auto const& path = paths[path_id];
			for (PathEdge path_edge : path) {
				if (random_edge_id == path_edge.edge_id) {
					PathIntervall path_intervall(
					    path_id, path_edge.time_intervall);
					manual_path_intervalls.push_back(path_intervall);
				}
			}
		}

		auto ds_path_intervalls =
		    pathfinder_ds.getPathIntervalls(random_edge_id);

		std::sort(manual_path_intervalls.begin(), manual_path_intervalls.end());
		std::sort(ds_path_intervalls.begin(), ds_path_intervalls.end());
		Test(ds_path_intervalls == manual_path_intervalls);
	}

	// test initTopNodes
	std::vector<bool> seen_edges(graph.getNumberOfEdges(), false);
	std::vector<bool> seen_nodes(graph.getNumberOfNodes(), false);

	auto mark_edges = [&](Edge const& edge, Direction direction) {
		if (pathfinder_ds.is_obsolete[edge.id]) {
			return CHTraversal::Result::Ignore;
		}
		seen_edges[edge.id] = true;
		Test(seen_nodes[edge.getTail(direction)]);
		seen_nodes[edge.getHead(direction)] = true;
		return CHTraversal::Result::Expand;
	};

	BoundingBox all_bounding_box({-90., -180.}, {90., 180.});
	auto const& top_nodes = pathfinder_ds.getTopNodes(all_bounding_box);
	for (auto node_id : top_nodes) {
		seen_nodes[node_id] = true;
		pathfinder_ds.traverse().down(mark_edges, node_id);
	}
	for (EdgeID edge_id = 0; edge_id < (EdgeID)seen_edges.size(); ++edge_id) {
		if (pathfinder_ds.is_obsolete[edge_id]) {
			Test(!seen_edges[edge_id]);
		} else {
			Test(seen_edges[edge_id]);
		}
	}
	for (auto seen : seen_nodes) {
		Test(seen);
	}

	printTestFooter("PathfinderDS");
}

void unit_tests::testCHTraversal()
{
	printTestHeader("CHTraversal");

	Graph graph;
	graph.init("../../data/test.graph");

	CHTraversal ch_traversal(graph);

	std::size_t edge_count;

	// test up
	edge_count = 0;
	auto push_up_function = [&](Edge const& edge, Direction direction) {
		++edge_count;
		Test(graph.isUpEdge(edge.id, direction));
	};
	ch_traversal.up(std::move(push_up_function));
	Test(edge_count == graph.getNumberOfEdges());

	// TODO: test up with a node_id

	// test down
	edge_count = 0;
	auto push_down_function = [&](Edge const& edge, Direction direction) {
		++edge_count;
		Test(graph.isDownEdge(edge.id, direction));
	};
	ch_traversal.down(std::move(push_down_function));
	Test(edge_count == graph.getNumberOfEdges());

	// TODO: test down with a node_id

	// test toParents
	edge_count = 0;
	auto push_to_parents_function = [&](EdgeID parent_id, EdgeID edge_id) {
		++edge_count;

		auto parents = graph.getParents(edge_id);
		auto it = std::find(parents.begin(), parents.end(), parent_id);
		Test(it != parents.end());
	};
	ch_traversal.toParents(std::move(push_to_parents_function));

	std::size_t number_of_children = 0;
	for (auto const& edge : graph.getAllEdges()) {
		if (edge.isShortcut()) {
			number_of_children += 2;
		}
	}

	Test(edge_count == number_of_children);

	printTestFooter("CHTraversal");
}

void unit_tests::testPathfinder(bool sortWRTObsolete)
{
	printTestHeader("Pathfinder");

	std::size_t number_of_queries = 100;

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);
	// only works with already partitioned random paths
	auto paths = path_generator.getRandomShortestPaths(100, 50);

	Print("Init pathfinder data structures");
	PathfinderDS pathfinder_ds(graph, nr_of_threads);
	pathfinder_ds.init(std::move(paths), false, sortWRTObsolete);
	Pathfinder pathfinder(pathfinder_ds);

	NaivePathfinder naive_pathfinder(pathfinder_ds);
	NaiveCHPathfinder naive_ch_pathfinder(pathfinder_ds);

	// test if output of find edge candidates is correct
	Print("Check findEdgeCandidates " << number_of_queries << " times");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto top_nodes = pathfinder_ds.getTopNodes(random_box);
		auto edge_ids = pathfinder.findEdgeCandidates(
		    random_box, c::FULL_INTERVALL, TimeSlots(true), top_nodes);
		EdgeIDs edge_ids_naive;
		for (auto const& edge : graph.getAllEdges()) {
			if (!pathfinder_ds.isContainedInPath(edge.id)) {
				continue;
			}
			auto edge_box = pathfinder_ds.getEdgeBox(edge.id);
			edge_box.intersect(random_box);

			if (edge_box.isValid()) {
				edge_ids_naive.push_back(edge.id);
			}
		}

		std::sort(edge_ids.begin(), edge_ids.end());
		std::sort(edge_ids_naive.begin(), edge_ids_naive.end());

		// Print("Query box: " << random_box);
		if (sortWRTObsolete) {
			Test(checkEquality(edge_ids, edge_ids_naive));
		}
	}

	Print("Issue " << number_of_queries << " queries");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto path_ids = pathfinder.run(random_box);
		auto path_ids_naive = naive_pathfinder.run(random_box);

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_naive.begin(), path_ids_naive.end());

		Test(checkEquality(path_ids, path_ids_naive));
	}

	Print("Issue " << number_of_queries << " queries with time constraint");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto random_intervall = random.getIntervall();
		auto path_ids =
		    pathfinder.run(random_box, random_intervall, TimeSlots(true));
		auto path_ids_naive =
		    naive_pathfinder.run(random_box, random_intervall, TimeSlots(true));

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_naive.begin(), path_ids_naive.end());

		Test(checkEquality(path_ids, path_ids_naive));
	}

	Print(
	    "Issue " << number_of_queries << " queries with time slot constraint");

	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto random_slots = random.getTimeSlots();
		auto path_ids =
		    pathfinder.run(random_box, c::FULL_INTERVALL, random_slots);
		auto path_ids_naive =
		    naive_pathfinder.run(random_box, c::FULL_INTERVALL, random_slots);

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_naive.begin(), path_ids_naive.end());

		Test(checkEquality(path_ids, path_ids_naive));
	}

	Print("Issue " << number_of_queries << " queries with naive CH pathfinder");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto path_ids = naive_ch_pathfinder.run(random_box);
		auto path_ids_naive = naive_pathfinder.run(random_box);

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_naive.begin(), path_ids_naive.end());

		Test(checkEquality(path_ids, path_ids_naive));
	}

	printTestFooter("Pathfinder");
}

void unit_tests::testFlags()
{
	printTestHeader("Flags");

	enum class TestFlags { Default, Value1, Value2 };
	std::size_t size = 10;

	Flags<TestFlags> flags(size, TestFlags::Default);

	flags.set(0, TestFlags::Default);
	Test(flags.changed_flags.empty());

	flags.set(3, TestFlags::Value2);
	flags.set(5, TestFlags::Value1);
	flags.set(7, TestFlags::Value1);
	Test(flags[3] == TestFlags::Value2);
	Test(flags[5] == TestFlags::Value1);
	Test(flags[7] == TestFlags::Value1);

	flags.set(7, TestFlags::Value2);
	Test(flags.changed_flags.size() == 3);

	flags.reset();
	Test(flags.changed_flags.empty());
	for (auto flag : flags.flags) {
		Test(flag == TestFlags::Default);
	}

	printTestFooter("Flags");
}

void unit_tests::testExporter()
{
	printTestHeader("Exporter");

	Graph graph;
	graph.init("../../data/test.graph");
	Geometry geometry(graph);

	PathGenerator path_generator(graph, random, nr_of_threads);
	auto paths = path_generator.getRandomPaths(100);

	Print("Init pathfinder data structures");
	PathfinderDS pathfinder_ds(graph, nr_of_threads);
	pathfinder_ds.init(std::move(paths), true);
	Pathfinder pathfinder(pathfinder_ds);

	Print("Run pathfinder query");
	auto random_box = random.getBox(graph);
	auto path_ids = pathfinder.run(random_box);

	Print("Export result");
	ExportData data;
	ExportColor blue{0, 0, 255, 255};
	ExportColor black{0, 0, 0, 255};

	data.filename = "exporter_testgraph.sg";
	data.boxes = {random_box};
	data.box_colors = {blue};
	for (auto path_id : path_ids) {
		data.paths.emplace_back(pathfinder_ds.getPaths()[path_id].unpack());
	}
	data.path_colors = ExportColors(data.paths.size(), black);

	Exporter exporter(graph);
	exporter.exportSimplestGraphRendererFile(data);

	printTestFooter("Exporter");
}

void unit_tests::testRTree()
{
	printTestHeader("RTree");

	RTree<int> r_tree;

	r_tree.emplace(BoundingBox({-2., 0.}, {-1., 1.}), 0);
	r_tree.emplace(BoundingBox({-1., 0.}, {0., 1.}), 1);
	r_tree.emplace(BoundingBox({0., 0.}, {1., 1.}), 2);
	r_tree.emplace(BoundingBox({1., 0.}, {2., 1.}), 3);
	r_tree.build();

	auto values = r_tree.search(BoundingBox({-.5, -.5}, {.5, .5}));

	Test(values.size() == 2);
	Test((values[0] == 1 && values[1] == 2) ||
	     (values[0] == 2 && values[1] == 1));

	printTestFooter("RTree");
}

void unit_tests::testAmbiguousShopas()
{
	printTestHeader("AmbiguousShopas");

	Graph graph;
	graph.init("../../data/test.graph");

	Path path1(graph);
	path1.push(44888);
	path1.push(44890);
	path1.push(44888);
	path1.push(44891);

	Path path2(graph);
	path2.push(44888);
	path2.push(44891);

	assert(graph.getEdge(44890).length == 0);
	assert(graph.getEdge(44888).length == 0);

	assert(path1.getLength() == path2.getLength());

	printTestFooter("AmbiguousShopas");
}

void unit_tests::testIntervalTree()
{
	printTestHeader("IntervalTree");

	// Test 1
	{
		IntervalTree<float, int> interval_tree;

		interval_tree.emplace(-1, 1, 1);
		interval_tree.emplace(3, 4, 2);
		interval_tree.emplace(-1.5, -0.5, 3);
		interval_tree.emplace(2.5, 4.5, 4);
		interval_tree.emplace(-3, -2, 5);
		interval_tree.emplace(0, 2, 6);
		interval_tree.emplace(-3, 4, 7);

		interval_tree.build();

		auto result = interval_tree.search(-0.1, 0.1);
		Test(result.size() == 3);
		std::sort(result.begin(), result.end());
		Test(result[0].value == 1);
		Test(result[1].value == 6);
		Test(result[2].value == 7);

		result = interval_tree.search(-2.5, -1.25);
		std::sort(result.begin(), result.end());
		Test(result.size() == 3);
		Test(result[0].value == 3);
		Test(result[1].value == 5);
		Test(result[2].value == 7);

		result = interval_tree.search(4.25, 4.75);
		std::sort(result.begin(), result.end());
		Test(result.size() == 1);
		Test(result[0].value == 4);
	}

	// Test 2
	{
		std::size_t number_of_intervals = 10000;
		std::size_t number_of_queries = 100;

		IntervalTree<int, int> interval_tree;

		TimeIntervalls time_intervals(number_of_intervals);
		for (std::size_t i = 0; i < number_of_intervals; ++i) {
			time_intervals[i] = random.getIntervall();
			interval_tree.emplace(
			    time_intervals[i].min_time, time_intervals[i].max_time, i);
		}

		interval_tree.build();

		for (std::size_t i = 0; i < number_of_queries; ++i) {
			auto query_interval = random.getIntervall();
			auto result_tree = interval_tree.search(
			    query_interval.min_time, query_interval.max_time);

			std::vector<int> result_tree_ids;
			for (auto result : result_tree) {
				result_tree_ids.push_back(result.value);
			}

			std::sort(result_tree_ids.begin(), result_tree_ids.end());

			std::vector<int> result_naive;
			for (std::size_t j = 0; j < time_intervals.size(); ++j) {
				TimeIntervall result_interval = query_interval;
				result_interval.intersect(time_intervals[j]);
				if (result_interval.isValid()) {
					result_naive.push_back(j);
				}
			}

			Test(result_tree_ids == result_naive);
		}
	}

	printTestFooter("IntervalTree");
}

void unit_tests::testMergeSortedUnique()
{
	printTestHeader("MergeSortedUnique");

	std::vector<std::vector<int>> sorted_vectors;
	for (int i = 0; i < 10; i++) {
		std::vector<int> sorted_vector;
		for (int i = 0; i < 1000; i++) {
			sorted_vector.push_back(random.getInt(0, 100000));
		}
		std::sort(sorted_vector.begin(), sorted_vector.end());
		sorted_vectors.push_back(sorted_vector);
	}

	std::vector<int> priority_queue_sorted =
	    merge_sorted_unique::by_priority_queue(sorted_vectors);

	std::vector<int> naively_sorted =
	    merge_sorted_unique::naive(sorted_vectors);

	std::vector<int> in_place_sorted =
	    merge_sorted_unique::inplace(sorted_vectors);

	Test(naively_sorted.size() == priority_queue_sorted.size());
	Test(naively_sorted.size() == in_place_sorted.size());
	for (uint i = 0; i < naively_sorted.size(); i++) {
		Test(naively_sorted.at(i) == priority_queue_sorted.at(i));
		Test(naively_sorted.at(i) == in_place_sorted.at(i));
	}

	printTestFooter("MergeSortedUnique");
}

} // namespace pf

// just in case anyone does anything stupid with this file...
#undef Test
