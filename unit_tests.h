#pragma once

#include "defs.h"

namespace pf
{

namespace unit_tests
{
void testAll(unsigned int seed);
void testAll();

void testAmbiguousShopas();

const uint nr_of_threads = 2;
} // namespace unit_tests

} // namespace pf
