#include "unit_tests.h"

#include "defs.h"

#include <string>

namespace
{

void printUsage()
{
	Print("USAGE: ./run_tests");
	Print("       ./run_tests <seed>");
}

} // anonymous namespace

int main(int argc, char* argv[])
{
	if (argc >= 3) {
		Print("ERROR: Too many arguments provided.");
		printUsage();
		return EXIT_FAILURE;
	}

	try {
		// without manual seed
		if (argc == 1) {
			pf::unit_tests::testAll();
		}
		// with manual seed
		if (argc == 2) {
			unsigned int seed;
			try {
				seed = std::stoul(argv[1]);
			} catch (std::invalid_argument const& e) {
				Print("ERROR: The seed has to be an unsigned int.");
				printUsage();
				return EXIT_FAILURE;
			}
			pf::unit_tests::testAll(seed);
		}
	} catch (Exception const& e) {
		std::cerr << e.what() << std::endl;
	}

	return EXIT_SUCCESS;
}
