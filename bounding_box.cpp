#include "bounding_box.h"

namespace pf
{

BoundingBox::BoundingBox()
{
}

BoundingBox::BoundingBox(Coordinate coordinate)
    : min_coordinate(coordinate), max_coordinate(coordinate)
{
}

BoundingBox::BoundingBox(Coordinate min_coordinate, Coordinate max_coordinate)
    : min_coordinate(min_coordinate), max_coordinate(max_coordinate)
{
}

BoundingBox::BoundingBox(Node const& node1, Node const& node2)
    : BoundingBox(node1.coordinate)
{
	expand(node2.coordinate);
}

BoundingBox::BoundingBox(Path const& path)
{
	auto const& graph = path.getGraph();

	for (PathEdge path_edge : path) {
		auto const& edge = graph.getEdge(path_edge.edge_id);
		auto const& source = graph.getNode(edge.source);
		auto const& target = graph.getNode(edge.target);

		expand(BoundingBox(source, target));
	}
}

bool BoundingBox::operator==(BoundingBox const& box) const
{
	return min_coordinate == box.min_coordinate &&
	       max_coordinate == box.max_coordinate;
}

bool BoundingBox::isValid() const
{
	return min_coordinate.isValid() && max_coordinate.isValid();
}

void BoundingBox::expand(BoundingBox const& box)
{
	if (!box.isValid()) {
		return;
	}
	if (!isValid()) {
		*this = box;
		return;
	}

	min_coordinate.minMerge(box.min_coordinate);
	max_coordinate.maxMerge(box.max_coordinate);
}

void BoundingBox::intersect(BoundingBox const& box)
{
	min_coordinate.maxMerge(box.min_coordinate);
	max_coordinate.minMerge(box.max_coordinate);

	if (min_coordinate.lon > max_coordinate.lon ||
	    min_coordinate.lat > max_coordinate.lat) {

		min_coordinate.setInvalid();
		max_coordinate.setInvalid();
	}
}

Coordinate BoundingBox::calcCenter() const
{
	auto lat_center = (min_coordinate.lat + max_coordinate.lat) / 2;
	auto lon_center = (min_coordinate.lon + max_coordinate.lon) / 2;
	return {lat_center, lon_center};
}

bool BoundingBox::contains(BoundingBox const& box) const
{
	return min_coordinate.lat <= box.min_coordinate.lat &&
	       min_coordinate.lon <= box.min_coordinate.lon &&
	       max_coordinate.lat >= box.max_coordinate.lat &&
	       max_coordinate.lon >= box.max_coordinate.lon;
}

std::ostream& operator<<(std::ostream& stream, BoundingBox const& box)
{
	stream << "min coordinate: " << box.min_coordinate
	       << ", max coordinate: " << box.max_coordinate;

	return stream;
}

} // namespace pf
