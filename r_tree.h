#pragma once

#include "bounding_box.h"

#include <algorithm>
#include <type_traits>
#include <vector>

namespace unit_tests { void testRTree(); }

template <typename Value>
class RTree
{
	static_assert(
	    std::is_pod<Value>::value, "The value parameter should be a POD type.");

	using ValueID = int32_t;
	using Values = std::vector<Value>;

	using RNodeID = int32_t;
	struct RNode {
		RNodeID id = std::numeric_limits<RNodeID>::max();
		BoundingBox box;
		ValueID value_id = std::numeric_limits<ValueID>::max();

		RNode() = default; // sets the box to invalid
		RNode(BoundingBox const& box, ValueID value_id)
		    : box(box), value_id(value_id)
		{
		}
		RNode(RNodeID id, BoundingBox const& box) : id(id), box(box)
		{
		}

		bool is_empty() const
		{
			return id == std::numeric_limits<RNodeID>::max();
		}
		bool is_leaf() const
		{
			return value_id != std::numeric_limits<ValueID>::max();
		}
	};
	using RNodes = std::vector<RNode>;

public:
	RTree() = default;

	void emplace(BoundingBox const& box, Value value);
	void build();
	void clear();

	Values search(BoundingBox box) const;

private:
	bool is_ready_for_search = false;

	RNodes r_nodes;
	Values values;

	struct BuildElement {
		RNodeID id;
		RNodeID begin;
		RNodeID end;

		BuildElement(RNodeID id, RNodeID begin, RNodeID end)
		    : id(id), begin(begin), end(end)
		{
		}
	};

	struct Comp {
		Comp(Dimension dimension) : dimension(dimension)
		{
		}

		bool operator()(RNode const& node1, RNode const& node2) const
		{
			auto center1 = node1.box.calcCenter();
			auto center2 = node2.box.calcCenter();
			return dimension == Dimension::Lat ? center1.lat < center2.lat
			                                   : center1.lon < center2.lon;
		};

	private:
		Dimension dimension;
	};

	Dimension calcSplitDimension(RNodeID begin, RNodeID end) const;
	BoundingBox calcBoundingBox(RNodeID begin, RNodeID end) const;
};

template <typename Value>
void RTree<Value>::emplace(BoundingBox const& box, Value value)
{
	// We only allow emplaces after construction or after a clear.
	assert(!is_ready_for_search);

	auto value_id = values.size();
	r_nodes.emplace_back(box, value_id);

	values.push_back(value);
}

template <typename Value>
void RTree<Value>::build()
{
	if (r_nodes.empty()) {
		return;
	}

	std::vector<BuildElement> build_stack;
	// push r_node with id 0 and the full range 0 ... r_nodes.size()
	build_stack.emplace_back(0, 0, r_nodes.size());

	// Give the points the correct IDs (which correspond to their
	// final position in the tree vector)
	while (!build_stack.empty()) {
		auto current = build_stack.back();
		build_stack.pop_back();

		assert(current.begin != current.end);
		// If only a single element remains in the range
		if (current.end - current.begin == 1) {
			r_nodes[current.begin].id = current.id;
			continue;
		}

		// Find median
		auto median_index = current.begin + (current.end - current.begin) / 2;
		auto median = r_nodes.begin() + median_index;
		auto begin = r_nodes.begin() + current.begin;
		auto end = r_nodes.begin() + current.end;
		auto split_dimension = calcSplitDimension(current.begin, current.end);
		std::nth_element(begin, median, end, Comp(split_dimension));

		// Create new inner node
		r_nodes.emplace_back(
		    current.id, calcBoundingBox(current.begin, current.end));

		// Find correct IDs of remainng elments (and check for empty range
		// before push)
		assert(current.begin < median_index);
		if (current.begin < median_index) {
			build_stack.emplace_back(
			    2 * current.id + 1, current.begin, median_index);
		}
		if (median_index < current.end) {
			build_stack.emplace_back(
			    2 * current.id + 2, median_index, current.end);
		}
	}

	// Build tree using the IDs
	// There are 3 steps to this:
	// 1) sort by ID (because that's the order in which the points will appear
	// in the tree vector)
	// 2) resize to largest id (as the tree vector has some gaps)
	// 3) actually place the points at their correct id (starting from the back
	// of the vector)

	// 1)
	auto sort_by_id = [](RNode const& node1, RNode const& node2) {
		assert(node1.id != node2.id);
		return node1.id < node2.id;
	};
	std::sort(r_nodes.begin(), r_nodes.end(), sort_by_id);
	// 2)
	auto number_of_points = r_nodes.size();
	r_nodes.resize(r_nodes.back().id + 1);
	// 3)
	for (int i = number_of_points - 1; i >= 0; --i) {
		std::swap(r_nodes[i], r_nodes[r_nodes[i].id]);
	}

	is_ready_for_search = true;
}

template <typename Value>
Dimension RTree<Value>::calcSplitDimension(RNodeID begin, RNodeID end) const
{
	assert(begin != end);

	Coordinate min(r_nodes[begin].box.min_coordinate);
	Coordinate max(r_nodes[begin].box.max_coordinate);

	for (auto i = begin; i < end; ++i) {
		min.minMerge(r_nodes[i].box.min_coordinate);
		max.maxMerge(r_nodes[i].box.max_coordinate);
	}

	return max.lat - min.lat > max.lon - min.lon ? Dimension::Lat
	                                             : Dimension::Lon;
}

template <typename Value>
BoundingBox RTree<Value>::calcBoundingBox(RNodeID begin, RNodeID end) const
{
	assert(begin != end);

	BoundingBox result_box(r_nodes[begin].box);
	for (auto i = begin; i < end; ++i) {
		result_box.expand(r_nodes[i].box);
	}

	return result_box;
}

template <typename Value>
auto RTree<Value>::search(BoundingBox query_box) const -> Values
{
	assert(is_ready_for_search);

	Values result;
	if (r_nodes.empty()) {
		return result;
	}

	std::vector<RNodeID> search_stack;
	search_stack.push_back(0);

	while (!search_stack.empty()) {
		auto r_node_id = search_stack.back();
		auto const& r_node = r_nodes[r_node_id];
		search_stack.pop_back();

		// ignore if r_node is empty
		if (r_node.is_empty()) {
			continue;
		}

		// ignore if intersection with bounding box is empty
		auto box = r_node.box;
		box.intersect(query_box);
		if (!box.isValid()) {
			continue;
		}

		if (r_node.is_leaf()) {
			result.push_back(values[r_node.value_id]);
		} else {
			// push first child
			assert(2 * r_node_id + 1 < (RNodeID)r_nodes.size());
			search_stack.push_back(2 * r_node_id + 1);

			// only push second child id if it exists
			if (2 * r_node_id + 2 < (RNodeID)r_nodes.size()) {
				search_stack.push_back(2 * r_node_id + 2);
			}
		}
	}

	return result;
}

template <typename Value>
void RTree<Value>::clear()
{
	r_nodes.clear();
	values.clear();

	is_ready_for_search = false;
}

} // namespace pf
