#pragma once

//
// 2D static range tree
//

// TODO rename: nodes, and value_element etc.

#include <algorithm>
#include <cassert>
#include <cmath>
#include <vector>

#include "id.h"

namespace unit_tests { void testRangeTree(); }

template <typename T, typename V>
class RangeTree
{
	static_assert(std::is_pod<T>::value, "The type parameter should be a POD type.");
	static_assert(std::is_pod<V>::value, "The value parameter should be a POD type.");

public:
	using Value = V;
	using Values = std::vector<Value>;
	struct Point { T x, y; };
	using Points = std::vector<Point>;
	struct Rect { T x_min, x_max, y_min, y_max; };

	void add(Point const& p, Value v);
	void build();
	void clear();

	void search(Rect const& query, Values& result);
	void searchAndDelete(Rect const& query, Values& result);

private:
	//
	// data
	//

	bool is_ready_for_search = false;

	struct PointValuePair;
	struct ValueEntry;
	struct Node;
	using PVPID = ID<PointValuePair>;
	using ValueEntryID = ID<ValueEntry>;
	using NodeID = ID<Node>;

	struct PointValuePair {
		Point point;
		Value value;

		PointValuePair(Point const& point, Value value)
			: point(point), value(value) {}
	};

	struct Node {
		T x;

		ValueEntryID begin;
		ValueEntryID end;

		Node() {} // id is invalid in this case

		bool is_empty() const { return !begin.valid() && !end.valid(); }
		bool is_leaf() const { return end-begin == 1; }
	};

	struct ValueEntry {
		T y;
		Value value;
		PVPID pvpid;
		ValueEntryID left;
		ValueEntryID right;

		ValueEntry(T y, Value value, PVPID pvpid) : y(y), value(value), pvpid(pvpid) {}

		bool operator<(ValueEntry const& other) const { return y < other.y; }
		bool operator<(T y) const { return this->y < y; }
		bool operator<=(T y) const { return this->y <= y; }
	};

	std::vector<PointValuePair> point_value_pairs;
	std::vector<Node> nodes;
	std::vector<ValueEntry> value_entries;
	std::vector<bool> deleted;

	//
	// helpers for searching
	//

	bool delete_reported;

	void search_impl(Rect const& query, Values& result);
	void addLeftValues(NodeID split_id, ValueEntryID value_entry_id, Rect const& query, Values& result);
	void addRightValues(NodeID split_id, ValueEntryID value_entry_id, Rect const& query, Values& result);
	void addToResult(ValueEntry const& value_entry, Values& result);

	//
	// helpers for building the tree
	//

	struct BuildElement {
		NodeID node_id;
		PVPID begin;
		PVPID end;

		BuildElement(NodeID node_id, PVPID begin, PVPID end)
			: node_id(node_id), begin(begin), end(end) {}
	};
	std::vector<BuildElement> todo;

	//
	// other helpers
	//

	static inline NodeID left(NodeID id) { return 2*id + 1; }
	static inline NodeID right(NodeID id) { return 2*id + 2; }
	static inline NodeID parent(NodeID id) { return (id-1)/2; }

	template <typename TT, typename VV>
	friend std::ostream& operator<<(std::ostream& os, RangeTree<TT, VV> const& tree);
};

template <typename T, typename V>
void RangeTree<T,V>::add(Point const& point, Value value)
{
	point_value_pairs.emplace_back(point, value);
	is_ready_for_search = false;
}

template <typename T, typename V>
void RangeTree<T,V>::build()
{
	if (point_value_pairs.empty()) { return; }

	deleted.assign(point_value_pairs.size(), false);

	nodes.reserve(point_value_pairs.size()*log2(point_value_pairs.size()));
	auto add_node = [this](NodeID node_id, ValueEntryID begin, ValueEntryID end) {
		if (node_id >= nodes.size()) {
			nodes.resize(node_id+1);
		}
		nodes[node_id].begin = begin;
		nodes[node_id].end = end;
	};

	// sort point_value_pairs by x (ascending)...
	auto compare_x = [](PointValuePair const& pvp1, PointValuePair const& pvp2) {
		return pvp1.point.x < pvp2.point.x;
	};
	std::sort(point_value_pairs.begin(), point_value_pairs.end(), compare_x);

	// set value entries for first node
	for (PVPID id = 0; id < point_value_pairs.size(); ++id) {
		auto const& pvp = point_value_pairs[id];
		value_entries.emplace_back(pvp.point.y, pvp.value, id);
	}
	std::sort(value_entries.begin(), value_entries.end());
	add_node(0, 0, value_entries.size());

	// now build the tree recursively
	todo.emplace_back(0, 0, point_value_pairs.size());

	while (!todo.empty()) {
		// get top
		NodeID node_id = todo.back().node_id;
		PVPID pvp_begin = todo.back().begin;
		PVPID pvp_end = todo.back().end;
		todo.pop_back();

		// set x value of node
		PVPID pvp_middle = pvp_begin + PVPID((pvp_end - pvp_begin + 1)/2);
		auto& node = nodes[node_id];
		node.x = point_value_pairs[pvp_middle-1].point.x;
		// if it is a leaf, we're done.
		if (pvp_end - pvp_begin == 1) { continue; }

		// push left child to todo and add value_entries incl. links
		auto value_entries_begin_left = value_entries.size();
		for (auto value_entry_id = node.begin; value_entry_id < node.end; ++value_entry_id) {
			auto& value_entry = value_entries[value_entry_id];
			value_entry.left = value_entries.size();
			if (value_entry.pvpid < pvp_middle) {
				value_entries.emplace_back(value_entry.y, value_entry.value, value_entry.pvpid);
			}
		}
		auto value_entries_end_left = value_entries.size();
		add_node(left(node_id), value_entries_begin_left, value_entries_end_left);
		// add x value to leaves and push non-leaves to todo
		if (pvp_middle - pvp_begin == 1) {
			nodes[left(node_id)].x = point_value_pairs[pvp_begin].point.x;
		}
		else {
			todo.emplace_back(left(node_id), pvp_begin, pvp_middle);
		}

		// same as above for right
		auto value_entries_begin_right = value_entries.size();
		for (auto value_entry_id = node.begin; value_entry_id < node.end; ++value_entry_id) {
			auto& value_entry = value_entries[value_entry_id];
			value_entry.right = value_entries.size();
			if (value_entry.pvpid >= pvp_middle) {
				value_entries.emplace_back(value_entry.y, value_entry.value, value_entry.pvpid);
			}
		}
		auto value_entries_end_right = value_entries.size();
		add_node(right(node_id), value_entries_begin_right, value_entries_end_right);
		// add x value to leaves and push non-leaves to todo
		if (pvp_end - pvp_middle == 1) {
			nodes[right(node_id)].x = point_value_pairs[pvp_middle].point.x;
		}
		else {
			todo.emplace_back(right(node_id), pvp_middle, pvp_end);
		}
	}

	is_ready_for_search = true;
}

template <typename T, typename V>
void RangeTree<T,V>::clear()
{
	value_entries.clear();
	nodes.clear();
	point_value_pairs.clear();
	deleted.clear();
	todo.clear();

	is_ready_for_search = false;
}

template <typename T, typename V>
void RangeTree<T,V>::search(Rect const& query, Values& result)
{
	delete_reported = false;
	search_impl(query, result);
}

template <typename T, typename V>
void RangeTree<T,V>::searchAndDelete(Rect const& query, Values& result)
{
	delete_reported = true;
	search_impl(query, result);
}

template <typename T, typename V>
void RangeTree<T,V>::search_impl(Rect const& query, Values& result)
{
	assert(is_ready_for_search);

	if (nodes.empty()) { return; }
	if (query.x_min > query.x_max || query.y_min > query.y_max) { return; }

	// Binary search for first relevant value_entry
	auto first_relevant = std::lower_bound(
		value_entries.begin(), value_entries.begin() + nodes[0].end, query.y_min);
	ValueEntryID value_entry_id = std::distance(value_entries.begin(), first_relevant);

	// There is no y value greater or equal to y_min in the tree.
	if (value_entry_id == value_entries.size()) { return; }

	// Search for split node
	NodeID current_id = 0;
	bool found_split_node = false;
	while (!found_split_node && current_id < nodes.size()) {
		auto const& current = nodes[current_id];

		// if the set of potential y values is already empty, return
		if (value_entry_id >= current.end ||
		    value_entries[value_entry_id].y > query.y_max) {
			return;
		}

		// if it is a leaf we have to check if we have to put its value into the result vector
		if (current.is_leaf()) {
			auto const& value_entry = value_entries[value_entry_id];
			if (current.x >= query.x_min && current.x <= query.x_max &&
			    value_entry.y >= query.y_min && value_entry.y <= query.y_max) {
				addToResult(value_entry, result);
			}
		}

		// if not, check if we go left or right or found one
		if (query.x_max <= current.x) {
			current_id = left(current_id);
			value_entry_id = value_entries[value_entry_id].left;
		}
		else if (query.x_min > current.x) {
			current_id = right(current_id);
			value_entry_id = value_entries[value_entry_id].right;
		}
		else { found_split_node = true; }
	}

	// if split node does not exist
	if (current_id >= nodes.size()) { return; }

	addLeftValues(left(current_id), value_entries[value_entry_id].left, query, result);
	addRightValues(right(current_id), value_entries[value_entry_id].right, query, result);
}

template <typename T, typename V>
void RangeTree<T,V>::addLeftValues(NodeID split_id, ValueEntryID value_entry_id, Rect const& query, Values& result)
{
	auto current_id = split_id;
	while (current_id < nodes.size()) {
		auto const& current = nodes[current_id];

		// if the set of potential y values is already empty, return
		if (value_entry_id >= current.end ||
		    value_entries[value_entry_id].y > query.y_max) {
			return;
		}

		if (current.is_leaf()) {
			auto y = value_entries[value_entry_id].y;
			if (current.x >= query.x_min && current.x <= query.x_max &&
				y >= query.y_min && y <= query.y_max) {
				addToResult(value_entries[value_entry_id], result);
			}
			return;
		}

		// step
		if (query.x_min <= current.x) {
			// first return values associated with right node
			if (right(current_id) < nodes.size()) {

				auto const& right_node = nodes[right(current_id)];
				if (right_node.is_empty()) { return; }

				auto it = value_entries.begin() + value_entries[value_entry_id].right;
				auto end = value_entries.begin() + right_node.end;

				while (it != end && *it <= query.y_max) {
					addToResult(*it, result);
					++it;
				}
			}

			current_id = left(current_id);
			value_entry_id = value_entries[value_entry_id].left;
		}
		else {
			current_id = right(current_id);
			value_entry_id = value_entries[value_entry_id].right;
		}
	}
}

template <typename T, typename V>
void RangeTree<T,V>::addRightValues(NodeID split_id, ValueEntryID value_entry_id, Rect const& query, Values& result)
{
	auto current_id = split_id;
	while (current_id < nodes.size()) {
		auto const& current = nodes[current_id];

		// if the set of potential y values is already empty, return
		if (value_entry_id >= current.end ||
		    value_entries[value_entry_id].y > query.y_max) {
			return;
		}

		// if it is a leaf
		if (current.is_leaf()) {
			auto y = value_entries[value_entry_id].y;
			if (current.x >= query.x_min && current.x <= query.x_max &&
			    y >= query.y_min && y <= query.y_max) {
				addToResult(value_entries[value_entry_id], result);
			}
			return;
		}

		// step
		if (query.x_max >= current.x) {
			// first return values associated with left node
			if (left(current_id) < nodes.size()) {

				auto const& left_node = nodes[left(current_id)];
				if (left_node.is_empty()) { return; }

				auto it = value_entries.begin() + value_entries[value_entry_id].left;
				auto end = value_entries.begin() + left_node.end;

				while (it != end && *it <= query.y_max) {
					addToResult(*it, result);
					++it;
				}
			}

			current_id = right(current_id);
			value_entry_id = value_entries[value_entry_id].right;
		}
		else {
			current_id = left(current_id);
			value_entry_id = value_entries[value_entry_id].left;
		}
	}
}

template <typename T, typename V>
void RangeTree<T,V>::addToResult(ValueEntry const& value_entry, Values& result)
{
	if (delete_reported) {
		if (!deleted[value_entry.pvpid]) {
			deleted[value_entry.pvpid] = true;
			result.push_back(value_entry.value);
		}
	}
	else {
		result.push_back(value_entry.value);
	}
}

template <typename T, typename V>
std::ostream& operator<<(std::ostream& os, RangeTree<T, V> const& tree)
{
	os << "number of nodes: " << tree.nodes.size() << "\n";
	os << "value entries size: " << tree.value_entries.size() << "\n";
	os << "\n";

	for (std::size_t node_id = 0; node_id < tree.nodes.size(); ++node_id) {
		auto const& node = tree.nodes[node_id];
		if (node.is_empty()) { continue; }

		os << "Node: " << node_id << " " << node.x << " " << node.begin << " " << node.end << "\n";

		auto begin = tree.value_entries.begin() + node.begin;
		auto end = tree.value_entries.begin() + node.end;
		os << "Value Entries:\n";
		for (auto it = begin; it != end; ++it) {
			os << it->y << " " << it->value << " " << it->left << " " << it->right << "\n";
		}
		os << "\n";
	}

	for (std::size_t id = 0; id < tree.value_entries.size(); ++id) {
		auto const value_entry = tree.value_entries[id];
		os << id << " " << value_entry.y << " " << value_entry.value << " " << value_entry.left << " " << value_entry.right << "\n";
	}

	return os;
}
