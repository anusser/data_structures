#pragma once

#include "defs.h"

#include <algorithm>
#include <cstdint>
#include <limits>
#include <type_traits>
#include <vector>

namespace unit_tests { void testIntervalTree(); }

template <typename Bound, typename Value>
class IntervalTree
{
	static_assert(
	    std::is_pod<Value>::value, "The value parameter should be a POD type.");
	using Values = std::vector<Value>;

	static constexpr int32_t invalid_value = -1;

	using IntervalID = int32_t;
	struct Interval {
		Bound begin;
		Bound end;
		Value value;

		Interval(Bound begin, Bound end, Value value)
		    : begin(begin), end(end), value(value)
		{
		}

		bool operator==(const Interval& rhs) const
		{
			return this->begin == rhs.begin && this->end == rhs.end &&
			       this->value == rhs.value;
		}

		bool operator<(const Interval& rhs) const
		{
			if (value < rhs.value) {
				return true;
			} else if (value > rhs.value) {
				return false;
			} else if (begin < rhs.begin) {
				return true;
			} else if (begin > rhs.begin) {
				return false;
			} else {
				return end < rhs.end;
			}
		}
	};
	using Intervals = std::vector<Interval>;

	struct IntervalRange {
		IntervalID begin = invalid_value;
		IntervalID end = invalid_value;

		IntervalRange() = default;
		IntervalRange(IntervalID begin, IntervalID end) : begin(begin), end(end)
		{
		}

		bool empty() const
		{
			return (begin == invalid_value && end == invalid_value) ||
			       (end <= begin);
		}
	};

	using NodeID = int32_t;
	using NodeIDs = std::vector<NodeID>;
	struct Node {
		Bound cut;
		IntervalRange begin_sorted;
		IntervalRange end_sorted;

		NodeID left = invalid_value;
		NodeID right = invalid_value;

		bool hasLeftChild() const
		{
			return left != invalid_value;
		}
		bool hasRightChild() const
		{
			return right != invalid_value;
		}
	};
	using Nodes = std::vector<Node>;

public:
	IntervalTree() = default;

	// This function pushes twice as we later need every interval twice in the
	// data structure.
	void emplace(Bound begin, Bound end, Value value)
	{
		if (begin <= end) {
			intervals.emplace_back(begin, end, value);
			intervals.emplace_back(begin, end, value);
			is_ready_for_search = false;
		}
	}
	void build();
	Intervals search(Bound begin, Bound end) const;

private:
	bool is_ready_for_search = true;

	Intervals intervals;
	Nodes nodes;

	// We want to make sure that the query can only modify the search stack
	// and nothing else from this class, therefore make this mutable.
	mutable NodeIDs search_stack;

	// functions and data used for building the interval tree
	struct BuildElement {
		NodeID node_id;
		IntervalRange range;

		BuildElement(NodeID node_id, IntervalRange range)
		    : node_id(node_id), range(range)
		{
		}
	};
	using BuildElements = std::vector<BuildElement>;

	std::vector<Bound> median_vec;
	Bound getMedian(BuildElement const& current);
	IntervalRange partitionIntervals(Bound cut, IntervalRange range);
	IntervalID sortBeginEndRanges(IntervalRange cut_range);
};

template <typename Bound, typename Value>
void IntervalTree<Bound, Value>::build()
{
	if (is_ready_for_search) {
		return;
	}
	BuildElements build_stack;
	nodes.emplace_back();
	build_stack.emplace_back(0, IntervalRange(0, intervals.size()));

	while (!build_stack.empty()) {
		auto current = build_stack.back();
		build_stack.pop_back();
		auto& node = nodes[current.node_id];

		node.cut = getMedian(current);
		auto cut_range = partitionIntervals(node.cut, current.range);

		auto middle = sortBeginEndRanges(cut_range);
		node.begin_sorted = {cut_range.begin, middle};
		node.end_sorted = {middle, cut_range.end};
		assert(!node.begin_sorted.empty());
		assert(!node.end_sorted.empty());

		// push children
		if (cut_range.begin != current.range.begin) {
			nodes.emplace_back();
			auto left_id = nodes.size() - 1;
			nodes[current.node_id].left = left_id;
			build_stack.emplace_back(
			    left_id, IntervalRange(current.range.begin, cut_range.begin));
		}
		if (cut_range.end != current.range.end) {
			nodes.emplace_back();
			auto right_id = nodes.size() - 1;
			nodes[current.node_id].right = right_id;
			build_stack.emplace_back(
			    right_id, IntervalRange(cut_range.end, current.range.end));
		}
	}

	is_ready_for_search = true;
}

template <typename Bound, typename Value>
auto IntervalTree<Bound, Value>::getMedian(BuildElement const& current) -> Bound
{
	// build vector of bounds
	median_vec.clear();
	for (auto i = current.range.begin; i < current.range.end; ++i) {
		auto const& interval = intervals[i];
		median_vec.push_back(interval.begin);
		median_vec.push_back(interval.end);
	}

	// find and return median
	auto middle = median_vec.size() / 2;
	std::nth_element(
	    median_vec.begin(), median_vec.begin() + middle, median_vec.end());

	// handle special case, is there a more natural option?
	if (median_vec.size() == 0 && middle == 0) {
		return -1;
	}
	return median_vec[middle];
}

template <typename Bound, typename Value>
auto IntervalTree<Bound, Value>::partitionIntervals(
    Bound cut, IntervalRange range) -> IntervalRange
{
	auto const cut_orientation = [cut](Interval const& i1, Interval const& i2) {
		// i1 left and i2 cut OR i1 cut and i2 right
		return (i1.end < cut && i2.end >= cut) ||
		       (i1.begin <= cut && i1.end >= cut && i2.begin > cut);
	};

	auto begin = intervals.begin() + range.begin;
	auto end = intervals.begin() + range.end;
	std::stable_sort(begin, end, cut_orientation);

	// find first cut interval
	auto is_not_left = [cut](
	    Interval const& interval) { return interval.end >= cut; };
	auto cut_begin_it = std::find_if(begin, end, is_not_left);
	// find first right interval
	auto is_right = [cut](
	    Interval const& interval) { return interval.begin > cut; };
	auto cut_end_it = std::find_if(begin, end, is_right);

	auto cut_begin = std::distance(intervals.begin(), cut_begin_it);
	auto cut_end = std::distance(intervals.begin(), cut_end_it);
	return IntervalRange(cut_begin, cut_end);
}

template <typename Bound, typename Value>
auto IntervalTree<Bound, Value>::sortBeginEndRanges(IntervalRange cut_range)
    -> IntervalID
{
	auto by_begin = [](
	    Interval const& i1, Interval const& i2) { return i1.begin < i2.begin; };
	auto by_end = [](
	    Interval const& i1, Interval const& i2) { return i1.end > i2.end; };
	auto middle = (cut_range.begin + cut_range.end) / 2;

	// first mix intervals such that one of each is in the first and second half
	// of the range
	for (IntervalID i = 0; i < middle - cut_range.begin; i += 2) {
		std::swap(intervals[cut_range.begin + i], intervals[middle + i]);
	}
	// then sort
	std::sort(intervals.begin() + cut_range.begin, intervals.begin() + middle,
	    by_begin);
	std::sort(
	    intervals.begin() + middle, intervals.begin() + cut_range.end, by_end);

	return middle;
}

template <typename Bound, typename Value>
auto IntervalTree<Bound, Value>::search(Bound begin, Bound end) const
    -> Intervals
{
	assert(is_ready_for_search);

	Intervals result;
	if (nodes.empty() || begin > end) {
		return result;
	}

	search_stack.clear();
	search_stack.push_back(0);
	while (!search_stack.empty()) {
		auto current_id = search_stack.back();
		search_stack.pop_back();
		auto const& current = nodes[current_id];

		// check node intervals for intersections
		// first check if we can just copy everything
		if ((begin <= current.cut) && (end >= current.cut)) {
			auto begin = intervals.begin() + current.begin_sorted.begin;
			auto end = intervals.begin() + current.begin_sorted.end;
			result.insert(result.end(), begin, end);
		} else if (begin <= current.cut) {
			for (auto i = current.begin_sorted.begin;
			     i < current.begin_sorted.end; ++i) {
				auto const& interval = intervals[i];
				if (interval.begin > end) {
					break;
				}
				result.push_back(interval);
			}
		} else if (end >= current.cut) {
			for (auto i = current.end_sorted.begin; i < current.end_sorted.end;
			     ++i) {
				auto const& interval = intervals[i];
				if (interval.end < begin) {
					break;
				}
				result.push_back(interval);
			}
		}

		// continue searching in children
		if (begin <= current.cut && current.hasLeftChild()) {
			search_stack.push_back(current.left);
		}
		if (end >= current.cut && current.hasRightChild()) {
			search_stack.push_back(current.right);
		}
	}

	return result;
}
